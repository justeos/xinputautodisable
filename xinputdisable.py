import os

device_to_disable = 'ELAN Touchscreen'

for s in os.popen('xinput').read().split('\n'):
    index = s.find(device_to_disable)
    if index > 0:
        s = s[s.find('id=') + 3:]
        s = s[:s.find('\t')]
        command = 'xinput disable ' + s
        print(command)
        out = os.popen(command).read()
